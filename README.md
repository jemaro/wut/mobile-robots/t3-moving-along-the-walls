# T3 Moving along the walls

Please, consider [reading this report
online](https://gitlab.com/jemaro/wut/mobile-robots/t3-moving-along-the-walls/-/blob/master/README.md).

### [Andreu Gimenez Bolinches](01163563@pw.edu.pl), student number: 317486

The main goal of the first tutorial is to learn to control a robot using
multiple behaviours governed by a finite state machine (FSM). 

## Solution
Comments can be find in the code that explain the solution step by step
```matlab
function [forwBackVel, leftRightVel, rotVel, finish] = solution3(...
        pts, contacts, position, orientation, varargin ...
        )
    % Initialize variables
    MAX_VEL = 5;
    MAX_ROT_VEL = 10;
    KP_R = 1;
    KP_ROT_VEL = 10; 

    % Initialize the robot control variables (returned by this function)
    finish = false;
    forwBackVel = 0;
    leftRightVel = 0;
    rotVel = 0;

    % Default input
    goal_theta = 0;
    sense = 1;
    smooth_edges = true;
    isdebug = false;

    switch length(varargin)
        case 0
            goal_rho = 1;
        case 1
            goal_rho = varargin{1};
        case 2
            [goal_rho, goal_theta] = deal(varargin{:});
        case 3
            [goal_rho, goal_theta, sense] = deal(varargin{:});
        case 4
            [goal_rho, goal_theta, sense, smooth_edges] = deal(varargin{:});
        case 5
            [goal_rho, goal_theta, sense, smooth_edges, isdebug] = ... 
                deal(varargin{:});
        otherwise
            error('Too many input arguments')
    end

    % Postprocess of input
    goal_theta = wrapToPi(goal_theta);
    % sense: 1 = left, -1 = right
    sense = 2 * (sense >= 0) - 1;

    % Finite State Machine initialization
    [INIT, MOVE] = assign_states();
    persistent state;

    if isempty(state)
        state = INIT;
    end

    % Finite State Machine code
    switch state
        case INIT
            state = MOVE;
            disp('Start moving now')
        case MOVE
            % Convert pts to polar coordinates
            [theta, rho] = cart2pol(pts(1, :), pts(2, :));
            % There is a rotation between the robot coordinate frame and the
            % sensor pointing, we want 0 to be the center, the face of the
            % sensor
            theta = theta + pi / 2;
            % Identify wall orthogonal points by finding local minimums
            walls = islocalmin([5 rho 5], 'MinProminence', 0.2);
            % Obviously this minimums should be contacts, points that are
            % inside the lidar range
            walls = and(walls(2:end - 1), contacts);
            % Compute discontinuities using the first derivative method. This
            % could be improved to be more robust using second derivative
            drho = [0, rho(2:end) - rho(1:end-1)];
            discontinuity = abs(drho) > 1;

            if isempty(walls)
                % Go forward until a wall is found
                leftRightVel = MAX_VEL;
            else
                wallidxs = find(walls);
                % Identify the front wall and its connected walls
                [~, idxs] = sort(rho(walls));
                % This is the index pointing to the front wall, the closest to
                % the robot
                target = wallidxs(idxs(1));
                % In order to smooth the interior corners, we want to detect a
                % second target. This target must be a wall connected to the
                % first
                if smooth_edges
                    for k = 2:length(idxs)
                        idx = wallidxs(idxs(k));
                        % In order to check if the second target is connected
                        % to the first, we check that all sensed points between
                        % the targets are 'contacts'. This works in our current
                        % environment as the distance between walls is big and
                        % the range sensor looses contact when there is an
                        % opening in the wall. It also checks if there is a
                        % discontinuity in the range sensor that may indicate
                        % an opening in the wall even if all points in between
                        % are contacts
                        between_idxs = min(target, idx):max(target, idx);
                        is_same_wall = all(contacts(between_idxs)) ... 
                            && ~any(discontinuity(between_idxs));
                        % Additionally we filter out points too far from the
                        % goal
                        is_close = rho(idx) <= 2*goal_rho ... 
                            && abs(angdiff(theta(idx), goal_theta)) <= 5*pi/8;
                        % If everything checks out we will add the target to
                        % our list of targets, making sure this list is ordered
                        % allways from right to left of the robot
                        if is_same_wall && is_close
                            target(2) = idx;
                            target = sort(target);
                            break;
                        end

                    end
                end

                % Now 'target' is a vector of length 1 or 2 that contains the
                % index of the target point or points in the array 'pts' and
                % vectors 'rho' and 'theta'

                %% Model as a moving circle
                if length(target) == 1
                    % The moving circle is just parallel to the wall
                    r_dir = theta(target);
                    error_rho = rho(target) - goal_rho;
                    % This are not needed unless we want to plot the moving
                    % circle
                    if isdebug
                        th0 = wrapToPi(r_dir + pi);
                        r0 = 2 * goal_rho - rho(target(1));
                    end
                else
                    % The moving circle is defined by two points in the wall.
                    % Knowing the radius and two points of the circle we can
                    % define its center. 
                    xa = (pts(1, target(2)) - pts(1, target(1))) / 2;
                    ya = (pts(2, target(2)) - pts(2, target(1))) / 2;
                    pt_0 = pts(1:2, target(1)) + [xa; ya];
                    a = sqrt(xa^2 + ya^2);
                    b = sqrt((2 * goal_rho)^2 - a^2);
                    center = pt_0 + [-ya; xa] * b / a;
                    [th0, r0] = cart2pol(center(1), center(2));
                    th0 = th0 + pi / 2;
                    % From the center we can extract which is the radial
                    % direction, the one that the robot should be moving
                    % tangent to
                    r_dir = wrapToPi(th0 + pi);
                    error_rho = goal_rho - r0;
                end

                % Once obtained the radial direction, we can calculate the
                % tangent one, the one that the robot should be usually moving
                % to. It can be changed by the option 'sense'
                t_dir = r_dir + sense * pi / 2; 
                % Compute the weight of the radial direction in the movement.
                % Depending on the error of the distance to the wall, we will
                % add more or less radial correction while keeping the same
                % absolute speed
                P = error_rho * KP_R;
                r = max(min(P, 1), -1);
                direction = r * r_dir + (1 - r) * t_dir;
                vel_L = MAX_VEL * [sin(direction) -cos(direction)];
                leftRightVel = vel_L(1);
                forwBackVel = vel_L(2);

                % Compute the necessary rotational velocity
                error_theta = angdiff(r_dir, goal_theta);
                P = error_theta * KP_ROT_VEL;
                rotVel = max(min(P, MAX_ROT_VEL), -MAX_ROT_VEL);

                %% Debug plot
                % Plot some variables usefull for debugging
                if isdebug
                    polarplot(theta(contacts), rho(contacts), '.');
                    hold on
                    rlim([0 5.5]);
                    % Discontinuities in the range sensor, openings
                    polarplot(theta(discontinuity), rho(discontinuity), 'bx');
                    % Moving circles
                    th = linspace(0, 2 * pi);
                    r = real(r0 .* cos(th - th0) ... 
                        + sqrt((2 * goal_rho)^2 - r0^2 .* sin(th - th0).^2));
                    polarplot(th, r, 'm--');
                    r = real(r0 .* cos(th - th0) ... 
                        + sqrt((goal_rho)^2 - r0^2 .* sin(th - th0).^2));
                    polarplot(th, r, 'm-');
                    % Targets
                    polarplot(theta(target), rho(target), 'r*');
                    % Where the target should be
                    polarplot(goal_theta, goal_rho, 'go');
                    % Where is the robot heading
                    polarplot(direction, goal_rho, 'gx');
                    hold off
                end

            end

        otherwise
            error('Unknown state %s.\n', state);
    end

end

function varargout = assign_states()
    % Assigns consecutive numeric values to the output arguments
    varargout = num2cell(1:nargout);
end
```

### Usage
+ Default parameters
```
run_simulation(@solution3, false)
```

+ All optional arguments
  Not all arguments need to be provided, if they are not provided, the default
  value will be used
```
d = 1; % Distance to keep respect to the wall
theta = 0 % Angle to maintain with respect to the wall, in radians
sense = 1 % Direction: -1 counterclockwise (left) | 1 clockwise (right)
smooth_edges = true; % Whether to smooth the edges of inner corners
isdebug = false; % Whether to plot some internal variables
run_simulation(@solution3, false, d, theta, sense, smooth_edges, isdebug)
```

## Results
+ Requisite
```
run_simulation(@solution3, false, 1, 0, 1, false, false)
```
![solution3_1](solution3_1.png)

+ Smooth edges
```
run_simulation(@solution3, false, 1, 0, 1, true, true)
```
![solution3_2](solution3_2.png)

+ Opposite direction keeping a different angle and distance
```
run_simulation(@solution3, false, 0.8, pi/6, -1, true, false)
```
![solution3_3](solution3_3.png)